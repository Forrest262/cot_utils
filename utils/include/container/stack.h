/**
  **********************************************************************************************************************
  * @file    stack.h
  * @brief   该文件提供单向栈LIFO所有函数原型
  * @author  const_zpc    any question please send mail to const_zpc@163.com
  * @version V0.2
  * @date    2023-05-29
  **********************************************************************************************************************
  *
  **********************************************************************************************************************
  */

/* Define to prevent recursive inclusion -----------------------------------------------------------------------------*/

#ifndef _COT_CONTAINER_STACK_H_
#define _COT_CONTAINER_STACK_H_

/* Includes ----------------------------------------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

#ifdef __cplusplus
 extern "C" {
#endif 


typedef struct
{
    uint8_t *pBuf;
    size_t  itemSize;
    size_t  limit;
    size_t  idx;
} cotStack_t;

extern void cotStack_Init(cotStack_t *pStack, uint8_t *pBuf, size_t bufSize, size_t itemSize);

extern bool cotStack_Empty(cotStack_t *pStack);
extern bool cotStack_Full(cotStack_t *pStack);
extern size_t cotStack_Size(cotStack_t *pStack);

extern void *cotStack_Top(cotStack_t *pStack);

extern int cotStack_Push(cotStack_t *pStack, const void *pdata, size_t length);
extern int cotStack_Pop(cotStack_t *pStack);

extern void cotStack_Swap(cotStack_t *pStack1, cotStack_t *pStack2);



#ifdef __cplusplus
 }
#endif

#endif // !_COT_CONTAINER_STACK_H_
