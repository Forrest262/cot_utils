/**
  **********************************************************************************************************************
  * @file       vector.h
  * @author     const_zpc (any question please send mail to const_zpc@163.com)
  * @brief      该文件提供动态数组所有函数原型
  * @version    0.1
  * @date       2023-05-28
  **********************************************************************************************************************
  *
  **********************************************************************************************************************
  */

/* Define to prevent recursive inclusion -----------------------------------------------------------------------------*/

#ifndef _COT_CONTAINER_VECTOR_H_
#define _COT_CONTAINER_VECTOR_H_

/* Includes ----------------------------------------------------------------------------------------------------------*/
#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>


#ifdef __cplusplus
 extern "C" {
#endif 

typedef struct
{
    uint8_t *pBuf;
    size_t  itemSize;
    size_t  limit;
    size_t  sIdx;
    size_t  eIdx;
} cotVector_t;


extern void cotVector_Init(cotVector_t *pVector, uint8_t *pBuf, size_t bufSize, size_t itemSize);

extern int cotVector_Assign(cotVector_t *pVector, const void *pdata, size_t n);

extern void *cotVector_Front(cotVector_t *pVector);
extern void *cotVector_Back(cotVector_t *pVector);
extern void *cotVector_At(cotVector_t *pVector, size_t idx);
extern uint8_t *cotVector_Data(cotVector_t *pVector);

extern bool cotVector_Empty(cotVector_t *pVector);
extern size_t cotVector_Size(cotVector_t *pVector);

extern int cotVector_Clear(cotVector_t *pVector);
extern int cotVector_Insert(cotVector_t *pVector, size_t idx, const void *pdata);
extern int cotVector_InsertN(cotVector_t *pVector, size_t idx, const void *pdata, size_t n);
extern int cotVector_Remove(cotVector_t *pVector, size_t idx);
extern int cotVector_RemoveN(cotVector_t *pVector, size_t idx, size_t n);
extern int cotVector_RemoveIf(cotVector_t *pVector, bool (*pfnCondition)(const void *pData));

extern int cotVector_Push(cotVector_t *pVector, const void *pdata);
extern int cotVector_Pop(cotVector_t *pVector);

extern void cotVector_Swap(cotVector_t *pVector1, cotVector_t *pVector2);

#ifdef __cplusplus
 }
#endif


#endif // !_COT_CONTAINER_VECTOR_H_

